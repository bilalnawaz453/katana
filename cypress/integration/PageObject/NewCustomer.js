class NewCustomer {

    clickAddGlobal() {

cy.get('#globalAdd').click();
return this;
   
};

clickAddCustomer() {

    cy.get('#add-customer').click()
    return this;
       
    };


    clickDisplayName() {

        cy.get('[data-testid="inputCustomerDisplayName"] > .MuiInputBase-root > .MuiInputBase-input').click()
        return this;
           
        };

       enterDisplayName() {

            cy.get('[data-testid="inputCustomerDisplayName"] > .MuiInputBase-root > .MuiInputBase-input').type('TestCustomer')
            return this;
               
            };
            
            clickInfoBilling() {

                cy.get('[data-testid="inputCustomerDefaultBillingAddress"] > .MuiInputBase-root > .MuiInputBase-input').click()
                return this;
                   
                };

                enterFirstName() {

                    cy.get('.MuiGrid-spacing-xs-6 > :nth-child(1) > :nth-child(1) > .MuiInputBase-root > .MuiInputBase-input').type('TestCustomer1')
                    return this;
                       
                    };

                    clickOkButton() {

                        cy.get('#submitButton').click()
                        return this;
                           
                        };

   
};
export default NewCustomer