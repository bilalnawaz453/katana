class SearchCustomer {

    clickSearchBttn() {

        cy.get('[data-testid="nameFilterInput"]').click();
return this;
    };

    searchCustomer() {

        cy.get('[data-testid="nameFilterInput"]').type('TestCustomer');
        
return this;
    };

};
export default SearchCustomer
