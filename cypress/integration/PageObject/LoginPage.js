class LoginPage {

    navigate() {
        cy.visit('https://katanamrp.com/login/')
    }

    enterEmail(username) {
        cy.get('.auth0-lock-input-email > .auth0-lock-input-wrap')
            .type(username);
        return this
    }

    enterPassword(pswd) {
        cy.get('.auth0-lock-input-show-password > .auth0-lock-input-block > .auth0-lock-input-wrap > .auth0-lock-input')
            .type(pswd)
        return this
    }
    submit() {
        cy.get('.auth0-lock-submit').click()
    }
 
}

export default LoginPage