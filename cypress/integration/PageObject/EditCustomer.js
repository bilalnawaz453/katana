class EditCustomer {

    clickEditContacts() {

        cy.get('#contactsTab > .MuiTab-wrapper').click();
return this;
    };

clickEditCustomerInfo() {

    cy.get('.ag-row-first > [aria-colindex="3"] > .ag-react-container > [data-testid="cellName"]').click('center', { force: true });
return this;
};

editFirstName() {

    cy.get('[data-testid="inputCustomerFirstName"] > .MuiInputBase-root > .MuiInputBase-input').type('XYZ');
    return this;
       
    };

    clickCancelBttn()
    {
        cy.get('.MuiButtonBase-root.print-hide > .MuiIconButton-label > .MuiSvgIcon-root').click();
        return this;
    }

};
export default EditCustomer