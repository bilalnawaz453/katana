/// <reference types="cypress" />
import LoginPage from "../integration/PageObject/LoginPage"
import NewCustomer from "../integration/PageObject/NewCustomer"
import EditCustomer from "../integration/PageObject/EditCustomer"
import SearchCustomer from "../integration/PageObject/SearchCustomer"

describe("Katana Test Case", function () {
    before(function () {
        cy.fixture('credentials').then(function (testdata) {
            this.testdata = testdata
        })
    })   
it("Login with valid credentials", function () {
    const login=new LoginPage();
    login.navigate();    
    login.enterEmail(this.testdata.username);
    login.enterPassword(this.testdata.password);
    login.submit();
    cy.url().should('be.equal', 'https://factory.katanamrp.com/sales')
});

it("Verify to add and Save Customer Info",function() {
    const addnew=new NewCustomer();
    addnew.clickAddGlobal();
    addnew.clickAddCustomer();
    cy.url().should('be.equal', 'https://factory.katanamrp.com/customer');
    addnew.clickDisplayName();
  addnew.enterDisplayName();
  addnew.clickInfoBilling();
  addnew.enterFirstName();
  addnew.clickOkButton();
    });
    it("Verify the Edit Customer Info",function() {
        const editInfo=new EditCustomer();
        editInfo.clickEditContacts();
        editInfo.clickEditCustomerInfo();
        editInfo.editFirstName();      
        });


        it("Verify the Cancel Butten",function() {
            const cancelbttn=new EditCustomer();
            cancelbttn. clickCancelBttn();
                 
            });


            it("Verify the Search functionality",function() {
                const searchCust=new SearchCustomer();
                var searchKeyword = "TestCustomer";
                searchCust.clickSearchBttn();
                searchCust.searchCustomer();
                cy.contains(searchKeyword);
                
                });
    

  

 
});

